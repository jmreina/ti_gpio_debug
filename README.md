# AM2434 GPIO Demo Debug

Get these demos working **together!** ([Ref](https://gitlab.com/jmreina/ti_gpio_debug))

## Demo Goal

1. SW5 Interrupt fires ISR
2. LD1 toggles @ 1 Hz in this ISR
3. ISR exits when SW5 is pressed again

# Contents

1. GPIO Read
2. GPIO Write
3. GPIO Interrupt

# Opens

1. Get read/write/interrupt working in same project!!
2. Get both LEDs to work in demo? (*LD1 & LD2*)
2. Get this to work reliably for driver reference

# Explicit Procedure notes

- Reset Board ([Ref](file:///C:/ti/mcu_plus_sdk_am243x_08_02_00_31/docs/api_guide_am243x/EVM_SETUP_PAGE.html))

	◦ Flash SOC Initialization Binary.AM243X-LP ('*Flash SOC Initialization Binary.AM243X-LP.png*')

- Import gpio_led_blink example (*confirm write*)
- Import gpio_input_interrupt (*confirm gpio-int*)
- Import gpio_input_interrupt as gpio_input (*confirm read*) ([Ref](https://e2e.ti.com/support/microcontrollers/arm-based-microcontrollers-group/arm-based-microcontrollers/f/arm-based-microcontrollers-forum/1088774/lp-am243-trouble-with-reading-gpio))
	
	Bug1 ('*Power Cycle Bug.png*') 
	
- Include write demo into gpio_input_interrupt

	*Bug1 occurs a few times
	
	status: LED write does not work in interrupt?

	note: repositories for each project listed (*.git.zip*)
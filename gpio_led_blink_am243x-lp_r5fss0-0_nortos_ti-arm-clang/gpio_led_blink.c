/*
 *  Copyright (C) 2021 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <drivers/gpio.h>
#include <kernel/dpl/AddrTranslateP.h>
#include <kernel/dpl/DebugP.h>
#include <kernel/dpl/ClockP.h>
#include "ti_drivers_config.h"
#include "ti_drivers_open_close.h"
#include "ti_board_open_close.h"

/*
 * This example configures a GPIO pin connected to an LED on the EVM in
 * output mode.
 * The application toggles the LED on/off for 10 seconds and exits.
 */

void gpio_led_blink_main(void *args) {

    //Locals
    uint32_t    gpioBaseAddr_led;
    uint32_t    gpioBaseAddr_sw5;

    //Init
    Drivers_open();                         /* Open drivers to open the UART driver for console   */
    Board_driversOpen();

    DebugP_log("GPIO LED Blink Test Started ...\r\n");
    DebugP_log("LED will Blink for a bunch ....\r\n");

    /* Get addresses after translation translate */
    gpioBaseAddr_led = (uint32_t) AddrTranslateP_getLocalAddr(GPIO_LED_BASE_ADDR);
    gpioBaseAddr_sw5 = (uint32_t) AddrTranslateP_getLocalAddr(GPIO_SW5_BASE_ADDR);

    /* Set output pin */
    GPIO_setDirMode(gpioBaseAddr_led, GPIO_LED_PIN, GPIO_LED_DIR);

    /* Set input pin */
    GPIO_setDirMode(gpioBaseAddr_sw5, GPIO_SW5_PIN, GPIO_SW5_DIR);

    for(;;) {

        //Read Pin Input (SW5)
        uint32_t sw5_pin = GPIO_pinRead(gpioBaseAddr_sw5, GPIO_SW5_PIN);

        //Apply Pin Output (LD1)
        if(sw5_pin) {
            GPIO_pinWriteHigh(gpioBaseAddr_led, GPIO_LED_PIN);
            DebugP_log("GPIO LED On\r\n");
        } else {
            GPIO_pinWriteLow(gpioBaseAddr_led, GPIO_LED_PIN);
            DebugP_log("GPIO LED Off\r\n");
        }

        //Loop Delay
        ClockP_sleep(1);
    }

    DebugP_log("GPIO LED Blink Test Passed!!\r\n");
    DebugP_log("All tests have passed!!\r\n");

    Board_driversClose();
    Drivers_close();
}

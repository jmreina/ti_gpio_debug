################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: Arm Compiler'
	"C:/ti/ccs1110/ccs/tools/compiler/ti-cgt-armllvm_1.3.0.LTS/bin/tiarmclang.exe" -c -mcpu=cortex-r5 -mfloat-abi=hard -mfpu=vfpv3-d16 -mlittle-endian -I"C:/ti/ccs1110/ccs/tools/compiler/ti-cgt-armllvm_1.3.0.LTS/include/c" -I"C:/ti/mcu_plus_sdk_am243x_08_02_00_31/source" -DSOC_AM243X -D_DEBUG_=1 -gstrict-dwarf -g -Wall -Wno-gnu-variable-sized-type-not-at-end -Wno-unused-function -MMD -MP -MF"$(basename $(<F)).d_raw" -MT"$(@)" -I"C:/Users/justin.reina/Documents/MyWorkspaces/CCS/GPIO/gpio_input_am243x-lp_r5fss0-0_nortos_ti-arm-clang/Debug/syscfg"  $(GEN_OPTS__FLAG) -o"$@" "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

build-1917269135: ../example.syscfg
	@echo 'Building file: "$<"'
	@echo 'Invoking: SysConfig'
	"C:/ti/ccs1110/ccs/utils/sysconfig_1.11.0/sysconfig_cli.bat" -s "C:/ti/mcu_plus_sdk_am243x_08_02_00_31/.metadata/product.json" --script "C:/Users/justin.reina/Documents/MyWorkspaces/CCS/GPIO/gpio_input_am243x-lp_r5fss0-0_nortos_ti-arm-clang/example.syscfg" --context "r5fss0-0" -o "syscfg" --part ALX --package ALX --compiler ticlang
	@echo 'Finished building: "$<"'
	@echo ' '

syscfg/ti_dpl_config.c: build-1917269135 ../example.syscfg
syscfg/ti_dpl_config.h: build-1917269135
syscfg/ti_drivers_config.c: build-1917269135
syscfg/ti_drivers_config.h: build-1917269135
syscfg/ti_drivers_open_close.c: build-1917269135
syscfg/ti_drivers_open_close.h: build-1917269135
syscfg/ti_pinmux_config.c: build-1917269135
syscfg/ti_power_clock_config.c: build-1917269135
syscfg/ti_board_config.c: build-1917269135
syscfg/ti_board_config.h: build-1917269135
syscfg/ti_board_open_close.c: build-1917269135
syscfg/ti_board_open_close.h: build-1917269135
syscfg/: build-1917269135

syscfg/%.o: ./syscfg/%.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: Arm Compiler'
	"C:/ti/ccs1110/ccs/tools/compiler/ti-cgt-armllvm_1.3.0.LTS/bin/tiarmclang.exe" -c -mcpu=cortex-r5 -mfloat-abi=hard -mfpu=vfpv3-d16 -mlittle-endian -I"C:/ti/ccs1110/ccs/tools/compiler/ti-cgt-armllvm_1.3.0.LTS/include/c" -I"C:/ti/mcu_plus_sdk_am243x_08_02_00_31/source" -DSOC_AM243X -D_DEBUG_=1 -gstrict-dwarf -g -Wall -Wno-gnu-variable-sized-type-not-at-end -Wno-unused-function -MMD -MP -MF"syscfg/$(basename $(<F)).d_raw" -MT"$(@)" -I"C:/Users/justin.reina/Documents/MyWorkspaces/CCS/GPIO/gpio_input_am243x-lp_r5fss0-0_nortos_ti-arm-clang/Debug/syscfg"  $(GEN_OPTS__FLAG) -o"$@" "$<"
	@echo 'Finished building: "$<"'
	@echo ' '


